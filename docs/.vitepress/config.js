module.exports = {
  title: "Документация",
  lang: "ru",
  base: "/docs/",
  outDir: "../public",

  themeConfig: {
    sidebar: getSidebar(),
    outlineTitle: "На этой странице",
    docFooter: {
      prev: "Предыдущая страница",
      next: "Следующая страница",
    },
  },
};

function getSidebar() {
  return [
    {
      text: "Пункт меню",
      items: [{ text: "Подпункт меню", link: "/menu/child" }],
    },
  ];
}
